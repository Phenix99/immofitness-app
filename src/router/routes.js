import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import Auth from "@/pages/Auth.vue";
import UserProfile from "@/pages/UserProfile.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Typography from "@/pages/Typography.vue";
import TableList from "@/pages/TableList.vue";
import ForfaitList from "@/pages/ForfaitList.vue";
import ServiceList from "@/pages/ServiceList.vue";
import RoleList from "@/pages/RoleList.vue";
import Accueil from "@/pages/Accueil.vue";
import VenteList from "@/pages/VenteList.vue";
import AdherentList from "@/pages/AdherentList.vue";
import Medecin from "@/pages/Medecin.vue";
import EmployeList from "@/pages/EmployeList.vue";
import StockList from "@/pages/StockList.vue";
import Facture from "@/pages/Facture.vue";

import store from '../store'

const OK_ROUTE = () => {
  let next_route = "";
  if (store.getters.isAdher || store.getters.isWorker) {
    next_route = "/accueil";
  } else if (store.getters.isAdmin || store.getters.isAssis) {
    next_route = "/ventes";
  } else if (store.getters.isDoctor) {
    next_route = "/medical";
  } else {
    next_route = false;
  }
  console.log(next_route);
  return next_route;
};

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/ventes')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
      console.log("Hello from ifAuthenticated")
      next()
      return;  
    }
    next('/connexion')
}

const isAdmin = (to, from, next) => {
  if (store.getters.isAdmin) {
      next()
      return
  }
  next('/ventes')
}

const isDoctor = (to, from, next) => {
  if (store.getters.isDoctor /* || store.getters.isAdmin */) {
      next()
      return
  }
  next('/connexion')
}

const isAssis = (to, from, next) => {
  if (store.getters.isAssis) {
      next()
      return
  }
  next('/connexion')
}

const isWorker = (to, from, next) => {
  if (store.getters.isWorker) {
      next()
      return
  }
  next('/connexion')
}
const isAdher = (to, from, next) => {
  if (store.getters.isAdher) {
      next()
      return
  }
  next('/connexion')
}

const isAdherOrIsWorker = (to, from, next) => {
  if (store.getters.isAdher || store.getters.isWorker) {
      next()
      return
  }
  next(OK_ROUTE())
}

const isAdminOrIsAssis = (to, from, next) => {
  if (store.getters.isAdmin || store.getters.isAssis) {
      next()
      return
  }
  next(OK_ROUTE())
}

const isNoRole = (to, from, next) => {
  if (store.getters.noRole) {
      next()
      return
  }
  next('/connexion')
}

const routes = [
  {
    path: "/connexion",
    component: Auth,
    beforeEnter: ifNotAuthenticated
  },
  {
    path: "/",
    component: DashboardLayout,
    beforeEnter: ifAuthenticated,
    redirect: "/connexion",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      },
      {
        path: "stats",
        name: "stats",
        component: UserProfile
      },
      {
        path: "accueil",
        name: "accueil",
        beforeEnter: isAdherOrIsWorker,
        component: Accueil
      },
      {
        path: "medical",
        name: "medical",
        beforeEnter: isDoctor,
        component: Medecin
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "icons",
        name: "icons",
        component: Icons
      },
      {
        path: "maps",
        name: "maps",
        component: Maps
      },
      {
        path: "typography",
        name: "typography",
        component: Typography
      },
      {
        path: "table-list",
        name: "table-list",
        component: TableList
      },
      {
        path: "forfaits",
        name: "forfaits",
        beforeEnter: isAdminOrIsAssis,
        component: ForfaitList
      },
      {
        path: "factures/:code",
        name: "factures",
        component: Facture
      },
      {
        path: "services",
        name: "services",
        beforeEnter: isAdminOrIsAssis,
        component: ServiceList
      },
      {
        path: "roles",
        name: "roles",
        beforeEnter: isAdmin,
        component: RoleList
      },
      {
        path: "ventes",
        name: "ventes",
        beforeEnter: isAdminOrIsAssis,
        component: VenteList
      },
      {
        path: "employes",
        name: "employes",
        beforeEnter: isAdmin,
        component: EmployeList
      },
      {
        path: "stocks",
        name: "stocks",
        beforeEnter: isAdminOrIsAssis,
        component: StockList
      },
      {
        path: "adherents",
        name: "adherents",
        beforeEnter: isAdminOrIsAssis,
        component: AdherentList
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
