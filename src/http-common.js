import axios from "axios";
let API_URL = process.env.VUE_APP_API_URL;

const http = axios.create({
  baseURL: API_URL,
  headers: {
    "Content-type": "application/json",
  },
});

/*
 * The interceptor here ensures that we check for the token in local storage every time an ajax request is made
 */
http.interceptors.request.use(
  (config) => {
    let api_key = localStorage.getItem("api_key");
    // let isOnline = localStorage.isOnline;
    if (api_key) {
      console.log("Authorization Token added !");
      config.headers["Authorization"] = /* `Bearer ${ api_key }` */ api_key;
    }
    // console.log('GRR', (isOnline))
    /* if(localStorage.isOnline) {
      console.log('IsOnline ! ' + localStorage.isOnline)
      return config;
    }
    else{
      console.log('IsOffline ! ' + localStorage.isOnline)
      UIkit.notification({
        message: `<span uk-icon='icon:'></span> You are offline !`,
        status: 'danger',
        pos: "top-right",
        timeout: 5000,
      });
      console.log('Notified !')
      throw new axios.Cancel('Operation canceled because network !');
    } */

    return config;
  },

  (error) => {
    return Promise.reject(error);
  }
);

export default http;
