/*!

 =========================================================
 * Vue Paper Dashboard - v2.0.0
 =========================================================

 * Product Page: http://www.creative-tim.com/product/paper-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from "vue";
import store from "./store";
import App from "./App";
import router from "./router/index";
import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";

import VueModal from '@kouts/vue-modal';
import '@kouts/vue-modal/dist/vue-modal.css';

import VueQrcodeReader from "vue-qrcode-reader";

import { ModelListSelect } from 'vue-search-select';
import 'vue-search-select/dist/VueSearchSelect.css';

import VueJsonPretty from 'vue-json-pretty'
import 'vue-json-pretty/lib/styles.css';

import PerfectScrollbar from 'vue-perfect-scrollbar'

import moment from 'moment'

moment.locale('fr')
Vue.prototype.moment = moment

Vue.component("vue-json-pretty", VueJsonPretty)

Vue.component('model-list-select', ModelListSelect)

Vue.component('perfect-scrollbar', PerfectScrollbar);
Vue.component('Modal', VueModal);

Vue.filter('fcfa', function (value) {
	return value + " FCFA"
})

Vue.filter('age', function (birthday) {
  var a = moment();
  var b = moment(birthday /* , "YYYY" */);
  var age = a.diff(b, "years"); // calculates patient's age in years
  return age; // this prints out the age
})

Vue.use(PaperDashboard);
Vue.use(VueQrcodeReader);

/* eslint-disable no-new */
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
