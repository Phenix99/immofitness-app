import Vue from 'vue';
import Vuex from 'vuex';
import user from'./modules/user';
import adherant from './modules/adherant';
import service from './modules/service';
import vente from './modules/vente';
import role from './modules/role';
import unite from './modules/unite';
import groupe from './modules/groupe';
import article from './modules/article';
import categorie from './modules/categorie';
import forfait from './modules/forfait';
import auth from './modules/auth';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    user,
    article,
    groupe,
    unite,
    role,
    adherant,
    categorie,
    service,
    vente,
    forfait,
  }
})