import axios from '@/http-common';

const state = {
  groupes: [],
  groupe:  {},
  data_search_groupe: [],
  last_message: ''
}

const getters = {
    groupe: state => state.groupe,
    groupes: state => state.groupes,
    dataSearchGroupe: state => state.data_search_groupe,
    getSelectedGroupe: state => state.groupe,
}

const mutations = {
    GET_ALL_GROUPES (state, payload) {
        state.groupes = payload;
        state.data_search_groupe = payload;
    },
    GET_GROUPE (state, payload) {
        state.groupe = payload;
    },
    CREATE_GROUPE (state, payload) {
        state.groupe = payload;
    },
    UPDATE_GROUPE (state, payload) {
        state.groupe = payload;
    },
    DELETE_GROUPE (state, payload) {
        state.last_message = payload;
    },
    ADD_GROUPE (state, payload) {
        state.groupes.push(payload);
    },
    REPLACE_GROUPE (state, payload) {
        state.groupes = state.groupes.map( groupe => {
            if(groupe.id === payload.id) return payload;
            else return groupe;
        })
    },
    REMOVE_GROUPE (state, payload) {
        state.groupes = state.groupes.filter( groupe => {
            return groupe.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_GROUPE (state, payload) {
        state.data_search_groupe = payload;
    },
    SELECT_GROUPE (state, payload) {
        state.groupe = payload;
    },
}

const actions = {
    getAllGroupes ({ commit }) {
        return axios.get(`/groupe/all`).then((response) => {
            commit('GET_ALL_GROUPES', response.data)
        });
    },
    getGroupe ({ commit }, id) {
        return axios.get(`/groupe/${id}`).then((response) => {
            commit('GET_GROUPE', response.data)
        });
    },
    createGroupe ({ commit }, data) {
        return axios.post(`/groupe/create`, data).then((response) => {
            commit('CREATE_GROUPE', response.data)
            commit('ADD_GROUPE', response.data)
        });
    },
    updateGroupe ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/groupe/update/${id}`, data).then((response) => {
            commit('UPDATE_GROUPE', response.data)
            commit('REPLACE_GROUPE', response.data)
        });
    },
    deleteGroupe ({ commit }, id) {
        return axios.delete(`/groupe/delete/${id}`).then((response) => {
            commit('DELETE_GROUPE', response.data)
            commit('REMOVE_GROUPE', id)
        });
    }
}

const groupeModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default groupeModule;