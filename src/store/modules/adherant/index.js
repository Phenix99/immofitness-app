import axios from '@/http-common';

const state = {
  adherants: [],
  adherant:  {},
  data_search_adherant: [],
  last_message: ''
}

const getters = {
    adherant: state => state.adherant,
    adherants: state => state.adherants.map(a=>{
        a.age = a.date_nais;
        return a;
    }),
    dataSearchAdherant: state => state.data_search_adherant,
    getSelectedAdherant: state => state.adherant,
    adherantById: (state) => (id) => state.adherants.find(adherant => adherant.id === id)
}

const mutations = {
    GET_ALL_ADHERANTS (state, payload) {
        state.adherants = payload;
        state.data_search_adherant = payload;
    },
    GET_ADHERANT (state, payload) {
        state.adherant = payload;
    },
    CREATE_ADHERANT (state, payload) {
        state.adherant = payload;
    },
    UPDATE_ADHERANT (state, payload) {
        state.adherant = payload;
    },
    DELETE_ADHERANT (state, payload) {
        state.last_message = payload;
    },
    ADD_ADHERANT (state, payload) {
        state.adherants.push(payload);
    },
    REPLACE_ADHERANT (state, payload) {
        payload.medical = JSON.stringify(payload.medical);
        state.adherants = state.adherants.map( adherant => {
            if(adherant.id === payload.id) return payload;
            else return adherant;
        })
        state.data_search_adherant = state.data_search_adherant.map( adherant => {
            if(adherant.id === payload.id) return payload;
            else return adherant;
        })
    },
    REMOVE_ADHERANT (state, payload) {
        state.adherants = state.adherants.filter( adherant => {
            return adherant.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_ADHERANT (state, payload) {
        state.data_search_adherant = payload;
    },
    SELECT_ADHERANT (state, payload) {
        state.adherant = payload;
    },
}

const actions = {
    getAllAdherants ({ commit }) {
        return axios.get(`/adherant/all`).then((response) => {
            commit('GET_ALL_ADHERANTS', response.data)
        });
    },
    getAdherant ({ commit }, id) {
        return axios.get(`/adherant/${id}`).then((response) => {
            commit('GET_ADHERANT', response.data)
        });
    },
    createAdherant ({ commit }, data) {
        return axios.post(`/adherant/create`, data).then((response) => {
            commit('CREATE_ADHERANT', response.data)
            commit('ADD_ADHERANT', response.data)
        });
    },
    updateAdherant ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/adherant/update/${id}`, data).then((response) => {
            commit('UPDATE_ADHERANT', response.data)
            commit('REPLACE_ADHERANT', response.data)
        });
    },
    deleteAdherant ({ commit }, id) {
        return axios.delete(`/adherant/delete/${id}`).then((response) => {
            commit('DELETE_ADHERANT', response.data)
            commit('REMOVE_ADHERANT', id)
        });
    }
}

const adherantModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default adherantModule;