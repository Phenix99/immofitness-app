import axios from '@/http-common';
import moment from 'moment';

const state = {
  ventes: [],
  modes: [],
  vente:  {start_date: moment().format("YYYY-MM-DD")},
  data_search_vente: [],
  last_message: ''
}

const getters = {
    vente: state => state.vente,
    ventes: state => state.ventes,
    modes: state => state.modes,
    dataSearchVente: state => state.data_search_vente,
    getSelectedVente: state => state.vente,
    venteById: (state) => (id) => state.ventes.find(vente => vente.id === id),
    venteByCode: (state) => (code) => state.ventes.find(vente => vente.code === code),
}

const mutations = {
    GET_ALL_VENTES (state, payload) {
        state.ventes = payload;
        state.data_search_vente = payload;
    },
    GET_ALL_MODES (state, payload) {
        state.modes = payload;
    },
    GET_ALL_TYPEFS (state, payload) {
        state.typefs = payload;
    },
    GET_VENTE (state, payload) {
        state.vente = payload;
    },
    CREATE_VENTE (state, payload) {
        state.vente = payload;
    },
    UPDATE_VENTE (state, payload) {
        state.vente = payload;
    },
    DELETE_VENTE (state, payload) {
        state.last_message = payload;
    },
    ADD_VENTE (state, payload) {
        state.ventes.push(payload);
    },
    REPLACE_VENTE (state, payload) {
        state.ventes = state.ventes.map( vente => {
            if(vente.id === payload.id) return payload;
            else return vente;
        })
    },
    REMOVE_VENTE (state, payload) {
        state.ventes = state.ventes.filter( vente => {
            return vente.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_VENTE (state, payload) {
        state.data_search_vente = payload;
    },
    SELECT_VENTE (state, payload) {
        state.vente = payload;
    },
}

const actions = {
    
    getAllVentes ({ commit }) {
        return axios.get(`/vente/all`).then((response) => {
            commit('GET_ALL_VENTES', response.data)
        });
    },
    getAllModes ({ commit }) {
        return axios.get(`/mode/all`).then((response) => {
            commit('GET_ALL_MODES', response.data)
        });
    },
    getVente ({ commit }, id) {
        return axios.get(`/vente/${id}`).then((response) => {
            commit('GET_VENTE', response.data)
        });
    },
    createVente ({ commit }, data) {
        return axios.post(`/vente/create`, data).then((response) => {
            commit('CREATE_VENTE', response.data)
            commit('ADD_VENTE', response.data)
        });
    },
    updateVente ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/vente/update/${id}`, data).then((response) => {
            commit('UPDATE_VENTE', response.data)
            commit('REPLACE_VENTE', response.data)
        });
    },
    deleteVente ({ commit }, id) {
        return axios.delete(`/vente/delete/${id}`).then((response) => {
            commit('DELETE_VENTE', response.data)
            commit('REMOVE_VENTE', id)
        });
    }
}

const venteModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default venteModule;