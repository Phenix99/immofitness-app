import axios from '@/http-common';

const state = {
  categories: [],
  categorie:  {},
  data_search_categorie: [],
  last_message: ''
}

const getters = {
    categorie: state => state.categorie,
    categories: state => state.categories.map(a=>{
        a.age = a.date_nais;
        return a;
    }),
    dataSearchCategorie: state => state.data_search_categorie,
    getSelectedCategorie: state => state.categorie,
    categorieById: (state) => (id) => state.categories.find(categorie => categorie.id === id)
}

const mutations = {
    GET_ALL_CATEGORIES (state, payload) {
        state.categories = payload;
        state.data_search_categorie = payload;
    },
    GET_CATEGORIE (state, payload) {
        state.categorie = payload;
    },
    CREATE_CATEGORIE (state, payload) {
        state.categorie = payload;
    },
    UPDATE_CATEGORIE (state, payload) {
        state.categorie = payload;
    },
    DELETE_CATEGORIE (state, payload) {
        state.last_message = payload;
    },
    ADD_CATEGORIE (state, payload) {
        state.categories.push(payload);
    },
    REPLACE_CATEGORIE (state, payload) {
        state.categories = state.categories.map( categorie => {
            if(categorie.id === payload.id) return payload;
            else return categorie;
        })
    },
    REMOVE_CATEGORIE (state, payload) {
        state.categories = state.categories.filter( categorie => {
            return categorie.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_CATEGORIE (state, payload) {
        state.data_search_categorie = payload;
    },
    SELECT_CATEGORIE (state, payload) {
        state.categorie = payload;
    },
}

const actions = {
    getAllCategories ({ commit }) {
        return axios.get(`/categorie/all`).then((response) => {
            commit('GET_ALL_CATEGORIES', response.data)
        });
    },
    getCategorie ({ commit }, id) {
        return axios.get(`/categorie/${id}`).then((response) => {
            commit('GET_CATEGORIE', response.data)
        });
    },
    createCategorie ({ commit }, data) {
        return axios.post(`/categorie/create`, data).then((response) => {
            commit('CREATE_CATEGORIE', response.data)
            commit('ADD_CATEGORIE', response.data)
        });
    },
    updateCategorie ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/categorie/update/${id}`, data).then((response) => {
            commit('UPDATE_CATEGORIE', response.data)
            commit('REPLACE_CATEGORIE', response.data)
        });
    },
    deleteCategorie ({ commit }, id) {
        return axios.delete(`/categorie/delete/${id}`).then((response) => {
            commit('DELETE_CATEGORIE', response.data)
            commit('REMOVE_CATEGORIE', id)
        });
    }
}

const categorieModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default categorieModule;