import axios from '@/http-common';

const state = {
  roles: [],
  role:  {},
  data_search_role: [],
  last_message: ''
}

const getters = {
    role: state => state.role,
    roles: state => state.roles,
    dataSearchRole: state => state.data_search_role,
    getSelectedRole: state => state.role,
    roleById: (state) => (id) => state.roles.find(role => role.id === id),
    rolesByCat: (state) => (categorie_id) => state.roles.filter(role =>  role.categorie_id == categorie_id)
}

const mutations = {
    GET_ALL_ROLES (state, payload) {
        state.roles = payload;
        state.data_search_role = payload;
    },
    GET_ROLE (state, payload) {
        state.role = payload;
    },
    CREATE_ROLE (state, payload) {
        state.role = payload;
    },
    UPDATE_ROLE (state, payload) {
        state.role = payload;
    },
    DELETE_ROLE (state, payload) {
        state.last_message = payload;
    },
    ADD_ROLE (state, payload) {
        state.roles.push(payload);
    },
    REPLACE_ROLE (state, payload) {
        state.roles = state.roles.map( role => {
            if(role.id === payload.id) return payload;
            else return role;
        })
    },
    REMOVE_ROLE (state, payload) {
        state.roles = state.roles.filter( role => {
            return role.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_ROLE (state, payload) {
        state.data_search_role = payload;
    },
    SELECT_ROLE (state, payload) {
        state.role = payload;
    },
}

const actions = {
    getAllRoles ({ commit }) {
        return axios.get(`/role/all`).then((response) => {
            commit('GET_ALL_ROLES', response.data)
        });
    },
    getRole ({ commit }, id) {
        return axios.get(`/role/${id}`).then((response) => {
            commit('GET_ROLE', response.data)
        });
    },
    createRole ({ commit }, data) {
        return axios.post(`/role/create`, data).then((response) => {
            commit('CREATE_ROLE', response.data)
            commit('ADD_ROLE', response.data)
        });
    },
    updateRole ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/role/update/${id}`, data).then((response) => {
            commit('UPDATE_ROLE', response.data)
            commit('REPLACE_ROLE', response.data)
        });
    },
    deleteRole ({ commit }, id) {
        return axios.delete(`/role/delete/${id}`).then((response) => {
            commit('DELETE_ROLE', response.data)
            commit('REMOVE_ROLE', id)
        });
    }
}

const roleModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default roleModule;