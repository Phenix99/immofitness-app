import axios from '@/http-common';

const state = {
  users: [],
  user:  {},
  data_search_user: [],
  last_message: ''
}

const getters = {
    users: state => state.users,
    user: state => state.user,
    dataSearchUser: state => state.data_search_user,
    dataSearchWorker: state => state.data_search_user.filter(user =>  user.role_id !== 4),
    getSelectedUser: state => state.user,
    workers: state => state.users.filter(user =>  user.role_id !== 4), 
    userById: (state) => (id) => state.users.find(user => user.id === id)
}

const mutations = {
    GET_ALL_USERS (state, payload) {
        state.users = payload;
        state.data_search_user = payload;
    },
    GET_USER (state, payload) {
        state.user = payload;
    },
    CREATE_USER (state, payload) {
        state.user = payload;
    },
    UPDATE_USER (state, payload) {
        state.user = payload;
    },
    DELETE_USER (state, payload) {
        state.last_message = payload;
    },
    ADD_USER (state, payload) {
        state.users.push(payload);
    },
    REPLACE_USER (state, payload) {
        state.users = state.users.map( user => {
            if(user.id === payload.id) return payload;
            else return user;
        })
        state.data_search_user = state.data_search_user.map( article => {
            if(user.id === payload.id) return payload;
            else return user;
        })
    },
    REMOVE_USER (state, payload) {
        state.users = state.users.filter( user => {
            return user.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_USER (state, payload) {
        state.data_search_user = payload;
    },
    SELECT_USER (state, payload) {
        state.user = payload;
    },
}

const actions = {
    getAllUsers ({ commit }) {
        return axios.get(`/user/all`).then((response) => {
            commit('GET_ALL_USERS', response.data)
        });
    },
    getUser ({ commit }, id) {
        return axios.get(`/user/${id}`).then((response) => {
            commit('GET_USER', response.data)
        });
    },
    createUser ({ commit }, data) {
        return axios.post(`/user/create`, data).then((response) => {
            commit('CREATE_USER', response.data)
            commit('ADD_USER', response.data)
        });
    },
    updateUser ({ commit }, {id, data}) {
        return axios.put(`/user/update/${id}`, data).then((response) => {
            commit('UPDATE_USER', response.data)
            commit('REPLACE_USER', response.data)
        });
    },
    deleteUser ({ commit }, id) {
        return axios.delete(`/user/delete/${id}`).then((response) => {
            commit('DELETE_USER', response.data)
            commit('REMOVE_USER', id)
        });
    }
}

const userModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default userModule;