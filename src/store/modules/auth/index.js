/* eslint-disable no-unused-vars */
import axios from '@/http-common';
// import NetworkHeartService from 'network-heart-service';

const BASE_URL = 'https://otti-auth-service.herokuapp.com/api'

const state = {
  appUser: JSON.parse(localStorage.getItem('user')) || {},
  api_key: localStorage.getItem('api_key') || '',
  isOnline: false,
}

const getters = {
    getAppUser: state => state.appUser,
    getAppAdherant: state => state.appUser.adherant,
    // getAppAdherantServices: state => state.appUser.adherant.services,
    // getAppAdherantForfaits: state => state.appUser.adherant.forfaits,
    getAppUserId: state => state.appUser.id,
    getApiKey: state => state.api_key,
    isAuthenticated: state => !!state.api_key,
    isAdmin: state => state.appUser.role_id == 1,
    isAssis: state => state.appUser.role_id == 2,
    isWorker: state => state.appUser.role_id == 3,
    isWorkerActive: state => state.appUser.actif == 1,
    isAdherActive: (state) => {
        if (!!state.appUser.adherant) return state.appUser.adherant.actif == 1;
        else return 0;
    },
    isAdher: state => state.appUser.role_id == 4,
    isDoctor: state => state.appUser.role_id == 5,
    noRole: state => !!state.appUser.role_id,
    hasAdher: state => !!state.appUser.adherant_id,
    /* getApiKey: (state) => (id) => {
      return state.productItems.find(productItem => productItem.id === id)
    } */ 
}

const mutations = {
    LOGIN (state, payload) {
      state.appUser = payload.user;
      state.api_key = payload.api_key;
      
      console.log(payload.api_key);
      localStorage.setItem('api_key', payload.api_key);
      localStorage.user_id = payload.user.id;
      localStorage.setItem('user', JSON.stringify(payload.user));
    },
    REGISTER (state, payload) {
        state.appUser = payload.user;
    },
    UPDATE_USER (state, payload) {
        state.appUser.adherant_id = payload.adherant_id;
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(state.appUser));
    },
    UPDATE_USER_ADHERANT (state, payload) {
        state.appUser.adherant = payload;
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(state.appUser));
    },
    CHANGE_NETWORK_STATUS (state, payload){
      state.isOnline = payload;
      localStorage.isOnline = payload;
    },
    DECREMENT_SERVICE (state, payload) {
        if(payload.id == state.appUser.adherant_id) {
            state.appUser.adherant = payload;
            localStorage.removeItem('user');
            localStorage.setItem('user', JSON.stringify(state.appUser));
        }
    },
    DECREMENT_FORFAIT (state, payload) {
        if(payload.id == state.appUser.adherant_id) {
            state.appUser.adherant = payload;
            localStorage.removeItem('user');
            localStorage.setItem('user', JSON.stringify(state.appUser));
        }
    },
    LOGOUT (state) {
        state.appUser = {};
        state.api_key = '';
        localStorage.removeItem('api_key') // if the request fails, remove any possible user token if possible
        localStorage.removeItem('user')
        delete localStorage.prev_active_link ;
    }
}

const actions = {
    login ({ commit }, user) {
        return new Promise((resolve, reject) => {
            axios.post(`/login`, user).then((response) => {
                commit('LOGIN', response.data)
                resolve(response)
            })
            .catch(err => {
                commit('LOGOUT')
                reject(err)
            })
        });
    },
    decrementServiceNumber ({ commit }, data) {
        return axios.post(`/service/decrement`, data).then((response) => {
            commit('DECREMENT_SERVICE', response.data)
            commit('UPDATE_ADHERANT', response.data)
            commit('REPLACE_ADHERANT', response.data)
        });
    },
    decrementForfaitNumber ({ commit }, data) {
        return axios.post(`/forfait/decrement`, data).then((response) => {
            commit('DECREMENT_FORFAIT',  response.data)
            commit('UPDATE_ADHERANT', response.data)
            commit('REPLACE_ADHERANT', response.data)
        });
    },
    register ({ commit }, user) {
        return new Promise((resolve, reject) => {
            axios.post(`/register`, user).then((response) => {
                commit('REGISTER', response.data)
                resolve(response)
            })
            .catch(err => {
                reject(err)
            })
        });
    },
    logout ({ commit }) {
        return new Promise((resolve, reject) => {
            commit('LOGOUT')
            resolve()
        });
    },
    startNetworkStatusService ({ commit }) {
        window.onLineHandler = function(){
            commit('CHANGE_NETWORK_STATUS',true)
            console.log('Connecté')
        };
        window.offLineHandler = function(){
            commit('CHANGE_NETWORK_STATUS',false)
            console.log('Hors connexion')
        };
    }
}

const userModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default userModule;