import axios from '@/http-common';

const state = {
  articles: [],
  article:  {},
  data_search_article: [],
  last_message: ''
}

const getters = {
    article: state => state.article,
    articles: state => state.articles,
    dataSearchArticle: state => state.data_search_article,
    getSelectedArticle: state => state.article,
    articlesByGroup: (state) => (groupe_id) => state.articles.filter(article =>  article.groupe_id == groupe_id),
    searchedArticlesByGroup: (state) => (groupe_id) => state.data_search_article.filter(article =>  article.groupe_id == groupe_id),
}

const mutations = {
    GET_ALL_ARTICLES (state, payload) {
        state.articles = payload;
        state.data_search_article = payload;
    },
    GET_ARTICLE (state, payload) {
        state.article = payload;
    },
    CREATE_ARTICLE (state, payload) {
        state.article = payload;
    },
    UPDATE_ARTICLE (state, payload) {
        state.article = payload;
    },
    DELETE_ARTICLE (state, payload) {
        state.last_message = payload;
    },
    ADD_ARTICLE (state, payload) {
        state.articles.push(payload);
    },
    REPLACE_ARTICLE (state, payload) {
        state.articles = state.articles.map( article => {
            if(article.id === payload.id) return payload;
            else return article;
        })
        state.data_search_article = state.data_search_article.map( article => {
            if(article.id === payload.id) return payload;
            else return article;
        })
    },
    REMOVE_ARTICLE (state, payload) {
        state.articles = state.articles.filter( article => {
            return article.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_ARTICLE (state, payload) {
        state.data_search_article = payload;
    },
    SELECT_ARTICLE (state, payload) {
        state.article = payload;
    },
}

const actions = {
    getAllArticles ({ commit }) {
        return axios.get(`/article/all`).then((response) => {
            commit('GET_ALL_ARTICLES', response.data)
        });
    },
    getArticle ({ commit }, id) {
        return axios.get(`/article/${id}`).then((response) => {
            commit('GET_ARTICLE', response.data)
        });
    },
    createArticle ({ commit }, data) {
        return axios.post(`/article/create`, data).then((response) => {
            commit('CREATE_ARTICLE', response.data)
            commit('ADD_ARTICLE', response.data)
        });
    },
    updateArticle ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/article/update/${id}`, data).then((response) => {
            commit('UPDATE_ARTICLE', response.data)
            commit('REPLACE_ARTICLE', response.data)
        });
    },
    deleteArticle ({ commit }, id) {
        return axios.delete(`/article/delete/${id}`).then((response) => {
            commit('DELETE_ARTICLE', response.data)
            commit('REMOVE_ARTICLE', id)
        });
    }
}

const articleModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default articleModule;