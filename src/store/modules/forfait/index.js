import axios from '@/http-common';

const state = {
  forfaits: [],
  typefs: [],
  forfait:  {},
  data_search_forfait: [],
  last_message: ''
}

const getters = {
    forfait: state => state.forfait,
    typefs: state => state.typefs,
    forfaits: state => state.forfaits,
    dataSearchForfait: state => state.data_search_forfait,
    getSelectedForfait: state => state.forfait,
    forfaitById: (state) => (id) => state.forfaits.find(forfait => forfait.id === id)
}

const mutations = {
    GET_ALL_FORFAITS (state, payload) {
        state.forfaits = payload;
        state.data_search_forfait = payload;
    },
    GET_ALL_TYPEFS (state, payload) {
        state.typefs = payload;
    },
    GET_FORFAIT (state, payload) {
        state.forfait = payload;
    },
    CREATE_FORFAIT (state, payload) {
        state.forfait = payload;
    },
    UPDATE_FORFAIT (state, payload) {
        state.forfait = payload;
    },
    DELETE_FORFAIT (state, payload) {
        state.last_message = payload;
    },
    ADD_FORFAIT (state, payload) {
        state.forfaits.push(payload);
    },
    REPLACE_FORFAIT (state, payload) {
        state.forfaits = state.forfaits.map( forfait => {
            if(forfait.id === payload.id) return payload;
            else return forfait;
        })
    },
    REMOVE_FORFAIT (state, payload) {
        state.forfaits = state.forfaits.filter( forfait => {
            return forfait.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_FORFAIT (state, payload) {
        state.data_search_forfait = payload;
    },
    SELECT_FORFAIT (state, payload) {
        state.forfait = payload;
    },
}

const actions = {
    getAllForfaits ({ commit }) {
        return axios.get(`/forfait/all`).then((response) => {
            commit('GET_ALL_FORFAITS', response.data)
        });
    },
    getAllTypefs ({ commit }) {
        return axios.get(`/typef/all`).then((response) => {
            commit('GET_ALL_TYPEFS', response.data)
        });
    },
    getForfait ({ commit }, id) {
        return axios.get(`/forfait/${id}`).then((response) => {
            commit('GET_FORFAIT', response.data)
        });
    },
    createForfait ({ commit }, data) {
        return axios.post(`/forfait/create`, data).then((response) => {
            commit('CREATE_FORFAIT', response.data)
            commit('ADD_FORFAIT', response.data)
        });
    },
    updateForfait ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/forfait/update/${id}`, data).then((response) => {
            commit('UPDATE_FORFAIT', response.data)
            commit('REPLACE_FORFAIT', response.data)
        });
    },
    deleteForfait ({ commit }, id) {
        return axios.delete(`/forfait/delete/${id}`).then((response) => {
            commit('DELETE_FORFAIT', response.data)
            commit('REMOVE_FORFAIT', id)
        });
    }
}

const forfaitModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default forfaitModule;