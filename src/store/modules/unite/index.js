import axios from '@/http-common';

const state = {
  unites: [],
  unite:  {},
  data_search_unite: [],
  last_message: ''
}

const getters = {
    unite: state => state.unite,
    unites: state => state.unites,
    dataSearchUnite: state => state.data_search_unite,
    getSelectedUnite: state => state.unite,
}

const mutations = {
    GET_ALL_UNITES (state, payload) {
        state.unites = payload;
        state.data_search_unite = payload;
    },
    GET_UNITE (state, payload) {
        state.unite = payload;
    },
    CREATE_UNITE (state, payload) {
        state.unite = payload;
    },
    UPDATE_UNITE (state, payload) {
        state.unite = payload;
    },
    DELETE_UNITE (state, payload) {
        state.last_message = payload;
    },
    ADD_UNITE (state, payload) {
        state.unites.push(payload);
    },
    REPLACE_UNITE (state, payload) {
        state.unites = state.unites.map( unite => {
            if(unite.id === payload.id) return payload;
            else return unite;
        })
    },
    REMOVE_UNITE (state, payload) {
        state.unites = state.unites.filter( unite => {
            return unite.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_UNITE (state, payload) {
        state.data_search_unite = payload;
    },
    SELECT_UNITE (state, payload) {
        state.unite = payload;
    },
}

const actions = {
    getAllUnites ({ commit }) {
        return axios.get(`/unite/all`).then((response) => {
            commit('GET_ALL_UNITES', response.data)
        });
    },
    getUnite ({ commit }, id) {
        return axios.get(`/unite/${id}`).then((response) => {
            commit('GET_UNITE', response.data)
        });
    },
    createUnite ({ commit }, data) {
        return axios.post(`/unite/create`, data).then((response) => {
            commit('CREATE_UNITE', response.data)
            commit('ADD_UNITE', response.data)
        });
    },
    updateUnite ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/unite/update/${id}`, data).then((response) => {
            commit('UPDATE_UNITE', response.data)
            commit('REPLACE_UNITE', response.data)
        });
    },
    deleteUnite ({ commit }, id) {
        return axios.delete(`/unite/delete/${id}`).then((response) => {
            commit('DELETE_UNITE', response.data)
            commit('REMOVE_UNITE', id)
        });
    }
}

const uniteModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default uniteModule;