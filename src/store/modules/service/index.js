import axios from '@/http-common';

const state = {
  services: [],
  service:  {},
  data_search_service: [],
  last_message: ''
}

const getters = {
    service: state => state.service,
    services: state => state.services,
    dataSearchService: state => state.data_search_service,
    getSelectedService: state => state.service,
    serviceById: (state) => (id) => state.services.find(service => service.id === id),
    servicesByCat: (state) => (categorie_id) => state.services.filter(service =>  service.categorie_id == categorie_id),
    searchedServicesByCat: (state) => (categorie_id) => state.data_search_service.filter(service =>  service.categorie_id == categorie_id),
}

const mutations = {
    GET_ALL_SERVICES (state, payload) {
        state.services = payload;
        state.data_search_service = payload;
    },
    GET_SERVICE (state, payload) {
        state.service = payload;
    },
    CREATE_SERVICE (state, payload) {
        state.service = payload;
    },
    UPDATE_SERVICE (state, payload) {
        state.service = payload;
    },
    DELETE_SERVICE (state, payload) {
        state.last_message = payload;
    },
    ADD_SERVICE (state, payload) {
        state.services.push(payload);
        // state.data_search_service.push(payload);
    },
    REPLACE_SERVICE (state, payload) {
        state.services = state.services.map( service => {
            if(service.id === payload.id) return payload;
            else return service;
        })
    },
    REMOVE_SERVICE (state, payload) {
        state.services = state.services.filter( service => {
            return service.id != payload;
        })
    },
    UPDATE_SEARCH_RESULT_SERVICE (state, payload) {
        state.data_search_service = payload;
    },
    SELECT_SERVICE (state, payload) {
        state.service = payload;
    },
}

const actions = {
    getAllServices ({ commit }) {
        return axios.get(`/service/all`).then((response) => {
            commit('GET_ALL_SERVICES', response.data)
        });
    },
    getService ({ commit }, id) {
        return axios.get(`/service/${id}`).then((response) => {
            commit('GET_SERVICE', response.data)
        });
    },
    createService ({ commit }, data) {
        return axios.post(`/service/create`, data).then((response) => {
            commit('CREATE_SERVICE', response.data)
            commit('ADD_SERVICE', response.data)
        });
    },
    updateService ({ commit }, {id, data}) {
      /* console.log(data);
      console.log(id) */
        return axios.put(`/service/update/${id}`, data).then((response) => {
            commit('UPDATE_SERVICE', response.data)
            commit('REPLACE_SERVICE', response.data)
        });
    },
    deleteService ({ commit }, id) {
        return axios.delete(`/service/delete/${id}`).then((response) => {
            commit('DELETE_SERVICE', response.data)
            commit('REMOVE_SERVICE', id)
        });
    }
}

const serviceModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default serviceModule;