#!/bin/bash

# Function to replace environment variables in a file
replace_env_vars() {
  local file="$1"
  echo "Processing: $file"

  # Get all environment variables starting with VUE_
  for env_var in $(env | grep '^VUE_' | cut -d= -f1); do
    # Create the corresponding VALUE pattern
    value_pattern="${env_var}_VALUE"

    # Check if the VALUE pattern exists in the file
    if grep -q "$value_pattern" "$file"; then
      echo "Found $value_pattern in $file, replacing with value from $env_var"
      sed -i "s|$value_pattern|${!env_var}|g" "$file"
    fi
  done
}

# Find and process all .js and .html files
find /usr/share/nginx/html -type f \( -name "*.js" -o -name "*.html" \) | while read file; do
  replace_env_vars "$file"
done

# Start nginx
nginx -g "daemon off;"
