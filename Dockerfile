# Stage 1: Build
FROM node:14-alpine as build

WORKDIR /app

# Copy package files and Docker-specific .npmrc
COPY package*.json ./

# Install dependencies without optional ones like Cypress
RUN npm install

# Copy source files and build
COPY . .
COPY .env.example .env
ENV NODE_ENV=production
RUN npm run build

# Stage 2: Production
FROM nginx:alpine

# Install necessary tools and make sure bash is properly linked
RUN apk add --no-cache bash findutils sed grep && \
  ln -sf /bin/bash /bin/sh && \
  mkdir -p /home

# Copy nginx configuration
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf

# Copy built files
COPY --from=build /app/dist /usr/share/nginx/html

# Copy and prepare the startup script
COPY start.sh /home/start.sh
RUN chmod +x /home/start.sh && \
  # Convert potential CRLF to LF
  dos2unix /home/start.sh || true && \
  # Verify the script exists and is executable
  ls -la /home/start.sh

EXPOSE 80

# Use full path to bash explicitly
ENTRYPOINT ["/bin/bash", "/home/start.sh"]
